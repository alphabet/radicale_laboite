from radicale_storage_mongo import radicale_mongo
from pymongo import MongoClient

# Override user ID used in Mongo
class Collection(radicale_mongo.Collection):

    @property
    def user_id(self):
        """Use the first part of the user address as user ID."""
        query = {'username': self._user_address}
        user = self._storage.database['users'].find_one(query)
        if user is not None:
            return user['_id']
        raise RuntimeError("Unknown user")

# Use new collection in storage
class Storage(radicale_mongo.Storage):
    Collection = Collection

# Expose radicale_mongo objects needed for all Radicale plugins
PLUGIN_CONFIG_SCHEMA = radicale_mongo.PLUGIN_CONFIG_SCHEMA
