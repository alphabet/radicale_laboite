# MongoDB storage plugin for Radicale

[Radicale] is a CalDAV and CardDAV server, for storing calendars and
contacts.  This python module provides an authentication plugin for Radicale
that authenticates to the authorization endpoint of an oauth2 server

[Radicale]: https://radicale.org/

## Installation

```shell
pip3 install radicale-storage-mongo
```

## Configuration

```in Radicale's INI
[storage]
type = radicale_mongo
db_url = mongodb://localhost/
db_name = meteor
collection_name = eventsAgenda
timezone = Europe/Paris

[web]
type = none

[logging]
level = debug
```
