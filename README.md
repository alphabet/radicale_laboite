# radicale_laboite

Dockerfile and configuration to build a Radicale image for laboite

to update mongo storage plugin :

- get file __init__.py from radicale_mongo project (https://gitlab.mim-libre.fr/alphabet/radicale_mongo)
- copy it as mongo/radicale_storage_mongo/radicale_mongo.py
- rebuild archive from mongo directory (rm mongo.tar.gz && tar -czf mongo.tar.gz mongo)
