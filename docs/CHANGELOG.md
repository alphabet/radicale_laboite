# Changelog

## [1.0.2](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/compare/release/1.0.1...release/1.0.2) (2022-09-20)


### Bug Fixes

* **authentication:** update keycloak plugin ([e045867](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/commit/e045867ba4306462b547fb9b123037f9a910a696))

## [1.0.1](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/compare/release/1.0.0...release/1.0.1) (2022-01-17)


### Continuous Integration

* **build:** create the docker image and push it to `${CI_REGISTRY}` ([a83b538](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/commit/a83b538f58244263ab952fd77f3217f9d4526dae))
* **commitlint:** enforce commit message format ([a0307fc](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/commit/a0307fcea96614ca6886b37298c681a7b1596745))
* **release:** avoid regression in `dev` branch ([9d3c342](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/commit/9d3c342c5eb7bc23d51641a5ce388825682bb986))
* **release:** create release automatically with `semantic-release` ([cf8bf23](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/commit/cf8bf239159eaf5a9f3402ec400e580fc9114555))
* **release:** tag docker images based on release cycle ([fe406e9](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/commit/fe406e9e95d6f60ef6535bd3b0feb687e418f30a))


### Documentation

* **contributing:** explain commit message format ([cfda93f](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/commit/cfda93fc0f545ddbd390aaddf0cafd9a04079b8a))
