# MongoDB storage plugin for Radicale

[Radicale] is a CalDAV and CardDAV server, for storing calendars and
contacts. This python module provides a storag plugin for Radicale
that gets calendar data from apps agenda mongodb database

[Radicale]: https://radicale.org/
[Apps]: https://gitlab.mim-libre.fr/alphabet/

## Installation

```shell
pip3 install radicale-storage-laboite
```

## Configuration

```in Radicale's INI
[storage]
type = radicale_storage_mongo
db_url = mongodb://mongo:27017
db_name = meteor
collection_name = eventsAgenda

[web]
type = none

[logging]
level = warning
```
