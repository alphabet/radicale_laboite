# Changelog

## [1.1.0](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/-/compare/release/1.0.2...release/1.1.0) (2024-01-30)


### Features

* **mongo:** update mongo storage plugin archive ([825976e](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/-/commit/825976e813922718aceb605553c794d48361558d))
* **radicale:** update radicale and alpine version ([fc97ee2](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/-/commit/fc97ee297fa129eed4d034017aa0d72cbb19ce1c))


### Bug Fixes

* **docker:** use eole docker proxy for alpine image ([176c69e](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/-/commit/176c69e97b7c8fdbfea182ca13aa6f471e4e85ba))
* **mongo storage:** update mongodb storage plugin ([ede304c](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/-/commit/ede304cbf758bd8f0a8463b4eee471220bd12cae))


### Reverts

* Revert "chore(ci): update CI configuration" ([3be4128](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/-/commit/3be412837b6428dc43bd7d3c68aebdde550b89e7))


### Continuous Integration

* **initial-checks:** replace obsolete `commitlint` job ([0e83f90](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/-/commit/0e83f90ba742e21df8eb5830fbfeec05a51db7e1))
* **release:** remove javascript specific code ([1878b7d](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/-/commit/1878b7d242ca3b281b7c6fee49a41c94ac81b244))
* **release:** stableBranch is master instead of main ([c42391e](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/-/commit/c42391eeeace97e8adf993c453a5f3c75b25f9e9))
* **release:** upgrade files ([009b94a](https://gitlab.mim-libre.fr/alphabet/radicale_laboite/-/commit/009b94a1cd04a8ae2a568b9fe71703c54b272d8c))
