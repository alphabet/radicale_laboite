#!/bin/bash
if [ "$1" == "purge" ]
then
    echo "* stop running containers (purge volumes)"
    docker-compose -f docker-compose-dev.yml down -v
else
    echo "* stop running containers"
    docker-compose -f docker-compose-dev.yml down
fi
echo "* Rebuild plugins"
rm -f storage_plugin.tar.gz
tar -czf storage_plugin.tar.gz storage_plugin
echo "* Create env file"
echo "TZ=\"Europe/Paris\"" > .env
echo "METEOR_SETTINGS=$(cat config/settings.development.json | jq -c .)" >> .env
echo "* Building ..."
docker-compose -f docker-compose-dev.yml up -d --build
# docker-compose -f docker-compose-dev.yml logs -f radicale
